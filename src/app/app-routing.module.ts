import { NgModule } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { Home2Component } from './home2/home2.component';


import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

	{ path: "home", component:HomeComponent },
	{ path: "home2", component:Home2Component },
	{ path: "**", redirectTo:"/", pathMatch: "full"}
	
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
